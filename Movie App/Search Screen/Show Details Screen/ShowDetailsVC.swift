//
//  ShowDetailsVC.swift
//  Movie App
//
//  Created by Alex Giatrakis on 28/09/2019.
//  Copyright © 2019 Alex Giatrakis. All rights reserved.
//

import UIKit
import SafariServices

final class ShowDetailsVC: UIViewController {
    
    var showResult: ShowResult!
    
    @IBOutlet private var showBackdropImageView: UIImageView!
    @IBOutlet private var showTitleLabel: UILabel!
    @IBOutlet private var summaryTitleLabel: UILabel!
    @IBOutlet private var showSummaryLabel: UILabel!
    @IBOutlet private var informationTitleLabel: UILabel!
    @IBOutlet private var showGenreLabel: UILabel!
    @IBOutlet private var showTrailerButton: UIButton!
    
    var youtubeURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        setupShowDetails()
        customShowDetails()
        getGenreNames()
        getYouTubeLink()
    }
    
    @IBAction func trailerButtonAction(sender: UIButton) {
        guard let youtubeURL = youtubeURL else { return }
        let safariController = SFSafariViewController(url: youtubeURL)
        self.present(safariController, animated: true)
    }

    func setupShowDetails() {
        
        //Show Backdrop
        showBackdropImageView.kf.indicatorType = .activity
        showBackdropImageView.kf.setImage(with: URL(string: showResult.backdropPath!), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
        
        //Show Title
        showTitleLabel.text = showResult.title

        //Show Summary
        showSummaryLabel.text = showResult.overview

        //Show Genre
        showGenreLabel.text = ""
        
        summaryTitleLabel.text = "Summary"
        informationTitleLabel.text = "Information"
    }
    
    func customShowDetails() {
        
        //Show Backdrop
        showBackdropImageView.contentMode = .scaleAspectFill
        
        //Show Title
        showTitleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 27)
        showTitleLabel.textColor = .white
        showTitleLabel.textAlignment = .center
        showTitleLabel.numberOfLines = 0
        
        //Show Summary
        showSummaryLabel.font = UIFont(name: "AvenirNext-DemiBoldItalic", size: 15)
        showSummaryLabel.textColor = .white
        showSummaryLabel.textAlignment = .center
        showSummaryLabel.numberOfLines = 0
        
        //Summary Title
        summaryTitleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 21)
        summaryTitleLabel.textColor = .white
        summaryTitleLabel.textAlignment = .center
        
        //Information Title
        informationTitleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 21)
        informationTitleLabel.textColor = .white
        informationTitleLabel.textAlignment = .center
       
        //Trailer Button
        showTrailerButton.isHidden = true
        showTrailerButton.backgroundColor = .red
        showTrailerButton.layer.cornerRadius = 10
        showTrailerButton.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 22)
        showTrailerButton.setTitle("Watch Trailer", for: .normal)
        showTrailerButton.setTitleColor(.white, for: .normal)
        
    }
    
    func getGenreNames() {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.themoviedb.org"
        components.path = "/3/\(showResult.mediaType ?? "")/\(showResult.id ?? -1)"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: "6b2e856adafcc7be98bdf0d8b076851c"),
            URLQueryItem(name: "language", value: "en-US")
        ]
        
        guard let url = components.url else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in

            guard let data = data else { return }

            do {
                let tempShow = try
                    JSONDecoder().decode(Show.self, from: data)

                let genreNames = tempShow.genres.compactMap { genre -> String? in
                    return genre.name
                }

                DispatchQueue.main.async {
                    self.informationTitleLabel.isHidden = false
                    self.showGenreLabel.text = genreNames.joined(separator: " / ")
                    self.showGenreLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15)
                    self.showGenreLabel.textColor = .white
                    self.showGenreLabel.textAlignment = .center
                    self.showGenreLabel.numberOfLines = 0
                }

            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }.resume()
        
    }
    
    func getYouTubeLink() {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.themoviedb.org"
        components.path = "/3/\(showResult.mediaType ?? "")/\(showResult.id ?? -1)/videos"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: "6b2e856adafcc7be98bdf0d8b076851c"),
            URLQueryItem(name: "language", value: "en-US")
        ]
        
        guard let url = components.url else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in

            guard let data = data else { return }

            do {
                let tempVideos = try
                    JSONDecoder().decode(VideoResults.self, from: data)

                guard let firstVideoKey = tempVideos.results.first?.key else { return }

                self.youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(firstVideoKey)")
                
                DispatchQueue.main.async {
                    self.showTrailerButton.isHidden = false
                }

            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }.resume()
    }
    

    
}
