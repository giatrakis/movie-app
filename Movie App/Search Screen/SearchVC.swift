//
//  ViewController.swift
//  Movie App
//
//  Created by Alex Giatrakis on 27/09/2019.
//  Copyright © 2019 Alex Giatrakis. All rights reserved.
//

import UIKit
import Kingfisher

final class SearchVC: UIViewController, UISearchBarDelegate {
    
    private var shows: ShowSearchResults?
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var searchBar: UISearchBar!
    @IBOutlet private var noResultsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Movie App"
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        self.tableView.backgroundColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        self.tableView.tableFooterView = UIView()
        
        //Search Bar
        self.searchBar.barTintColor = #colorLiteral(red: 0.1176470588, green: 0.1176470588, blue: 0.1176470588, alpha: 1)
        self.searchBar.placeholder = "Find Movies and TV Shows"
        self.searchBar.searchTextField.backgroundColor = .white
        self.searchBar.layer.borderWidth = 1
        self.searchBar.layer.borderColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        self.searchBar.frame.size.height = 44
        
        //No Results
        self.noResultsLabel.text = "Search for Movies and TV Shows"
        self.noResultsLabel.font = UIFont(name: "Avenir-Heavy", size: 28)
        self.noResultsLabel.textAlignment = .center
        self.noResultsLabel.numberOfLines = 0
        self.noResultsLabel.textColor = .gray
        self.view.bringSubviewToFront(noResultsLabel)
    }
    
    
    private func buildSearchURL(searchTerms string: String?) -> URL? {
        
        guard let string = string else { return nil }
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.themoviedb.org"
        components.path = "/3/search/multi"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: "6b2e856adafcc7be98bdf0d8b076851c"),
            URLQueryItem(name: "language", value: "en-US"),
            URLQueryItem(name: "query", value: string),
            URLQueryItem(name: "page", value: "1"),
            URLQueryItem(name: "include_adult", value: "false")
        ]

        return components.url        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let url = buildSearchURL(searchTerms: searchBar.text) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            
            guard let data = data else { return }
            
            do {
                var tempShows = try
                    JSONDecoder().decode(ShowSearchResults.self, from: data)
                
                for i in 0..<tempShows.results.count {
                    let tempPosterPath = "https://image.tmdb.org/t/p/w500" + (tempShows.results[i].posterPath ?? "")
                    let tempBackDropPath = "https://image.tmdb.org/t/p/w500" + (tempShows.results[i].backdropPath ?? "")
                    tempShows.results[i].posterPath = tempPosterPath
                    tempShows.results[i].backdropPath = tempBackDropPath
                }
                
                for i in 0..<tempShows.results.count {
                    if(tempShows.results[i].title == nil) {
                        let showTitle = tempShows.results[i].name
                        tempShows.results[i].title = showTitle
                    }
                }
                
                for i in 0..<tempShows.results.count {
                    if(tempShows.results[i].releaseDate == nil) {
                        let showFirstAirDate = tempShows.results[i].firstAirDate
                        tempShows.results[i].releaseDate = showFirstAirDate
                    }
                }

                self.shows = tempShows
                
                DispatchQueue.main.async {
                    if self.shows?.results.count == 0 {
                        self.noResultsLabel.alpha = 1.0
                        self.noResultsLabel.text = "No Results"
                    } else {
                        self.noResultsLabel.alpha = 0.0
                    }
                    
                    self.tableView.reloadData()
                    
                    if self.shows?.results.count ?? 0 > 0 {
                        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                    }
                }
                
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }.resume()
        
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchResultsToShowDetails" {
            let destVC = segue.destination as! ShowDetailsVC
            destVC.showResult = sender as? ShowResult
        }
    }
    
    
}




extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let results = shows?.results else { return UITableViewCell() }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell", for: indexPath) as! SearchResultTableViewCell
        
        //Show Title
        cell.showTitleLabel.text = results[indexPath.row].title
        
        //Show Vote
        if let unwrappedShowVote = results[indexPath.row].voteAverage {
            cell.showVoteLabel.text = "\(unwrappedShowVote)"
        }
        
        //Show Release Date
        cell.showReleaseDateLabel.text = String(results[indexPath.row].releaseDate?.prefix(4) ?? "Release Date Unavailable")
        
        //Show Poster
        cell.showPosterImageView.kf.indicatorType = .activity
        cell.showPosterImageView.kf.setImage(with: URL(string: (results[indexPath.row].posterPath)!), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
        
        return cell
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  shows?.results.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let show = shows?.results[indexPath.row] else { return }
        performSegue(withIdentifier: "SearchResultsToShowDetails", sender: show)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 235
    }
}
