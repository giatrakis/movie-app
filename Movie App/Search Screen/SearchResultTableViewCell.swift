//
//  SearchResultTableViewCell.swift
//  Movie App
//
//  Created by Alex Giatrakis on 27/09/2019.
//  Copyright © 2019 Alex Giatrakis. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {

    @IBOutlet var showPosterImageView: UIImageView!
    @IBOutlet var showTitleLabel: UILabel!
    @IBOutlet var showReleaseDateLabel: UILabel!
    @IBOutlet var showVoteLabel: UILabel!
    @IBOutlet var showVoteView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        selectionStyle = .none
        
        //Show Poster
        showPosterImageView.layer.cornerRadius = 5.0
        
        //Show Title
        showTitleLabel.font = UIFont(name: "AvenirNext-Medium", size: 21)
        showTitleLabel.textColor = .white
        showTitleLabel.numberOfLines = 0
        
        //Show Release Date
        showReleaseDateLabel.font = UIFont(name: "AvenirNext-Bold", size: 18)
        showReleaseDateLabel.textColor = .white
        showReleaseDateLabel.numberOfLines = 2
        
        //Show Vote
        showVoteLabel.font = UIFont(name: "AvenirNextCondensed-Heavy", size: 30)
        showVoteLabel.textColor = .white
        showVoteLabel.shadowColor = .black
        showVoteLabel.shadowOffset = CGSize(width: 1, height: 2)
        showVoteLabel.textAlignment = .center
        
        //Show Vote View
        showVoteView.backgroundColor = #colorLiteral(red: 0.8, green: 0.6862745098, blue: 0.168627451, alpha: 1)
        showVoteView.layer.cornerRadius = 5.0
        showVoteView.layer.shadowColor = UIColor.black.cgColor
        showVoteView.layer.shadowOffset = CGSize(width: 2, height: 2.0)
        showVoteView.layer.shadowRadius = 4.0
        showVoteView.layer.shadowOpacity = 0.8
        showVoteView.layer.masksToBounds = false
    }

}
