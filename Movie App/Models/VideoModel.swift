//
//  VideoModel.swift
//  Movie App
//
//  Created by Alex Giatrakis on 02/10/2019.
//  Copyright © 2019 Alex Giatrakis. All rights reserved.
//

import Foundation

struct Video: Decodable {
    let key: String
}

struct VideoResults: Decodable {
    let results: [Video]
}
