//
//  ShowGategory.swift
//  Movie App
//
//  Created by Alex Giatrakis on 27/09/2019.
//  Copyright © 2019 Alex Giatrakis. All rights reserved.
//

import UIKit

struct ShowResult: Decodable {
    
    var title: String?
    let name: String?
    let overview: String?
    var posterPath: String?
    var backdropPath: String?
    let voteAverage: Float?
    var releaseDate: String?
    let firstAirDate: String?
    let id: Int?
    let mediaType: String?
    
    enum CodingKeys: String, CodingKey {
        case title, name, overview, id
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case voteAverage = "vote_average"
        case releaseDate = "release_date"
        case firstAirDate = "first_air_date"
        case mediaType = "media_type"
    }
}

struct ShowSearchResults: Decodable {
    var results: [ShowResult]
}
