//
//  GenresModel.swift
//  Movie App
//
//  Created by Alex Giatrakis on 02/10/2019.
//  Copyright © 2019 Alex Giatrakis. All rights reserved.
//

import Foundation

struct Genres: Decodable {
    let name: String?
}

struct Show: Decodable {
    let genres: [Genres]
}
